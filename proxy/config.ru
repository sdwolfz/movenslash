# frozen_string_literal: true

require 'async/websocket/adapters/rack'

require 'logger'
require 'socket'
require 'timeout'

# ------------------------------------------------------------------------------

logger = Logger.new($stdout)

# ------------------------------------------------------------------------------

class SocketSingleton
  class << self
    def socket
      @socket ||= begin
        socket_ip   = '127.0.0.1'
        socket_port = 3000

        server_socket = UDPSocket.new
        server_socket.connect(socket_ip, socket_port)
        puts "UDP Socket open on #{socket_ip}:#{socket_port}"

        server_socket
      end
    end
  end
end

# ------------------------------------------------------------------------------

# Rack app to forward websocket requests to UPD socket
class WebSocketApp
  def initialize(logger:)
    @logger = logger
  end

  attr_reader :logger

  def call(env)
    if env['rack.protocol'] == ['websocket']
      Async::WebSocket::Adapters::Rack.open(env) do |connection|
        while (message = connection.read)
          value = message.to_str
          logger.debug("Received: #{value}")

          logger.debug("Sending to Server: #{value}")
          SocketSingleton.socket.send(value, 0)
          logger.debug("Sent to Server: #{value}")

          response, = SocketSingleton.socket.recvfrom(1024)
          logger.debug("Received from Server: #{response}")
          connection.write(response)
          logger.debug("Sent to Client: #{response}")
        end
      rescue Protocol::WebSocket::ClosedError
        logger.debug("Connection Closed: #{connection}")
      rescue StandardError => e
        logger.error(e)
      end
    else
      [200, { 'Content-Type' => 'text/plain' }, ['Hello! Try connecting via WebSocket!']]
    end
  end
end

run WebSocketApp.new(logger: logger)

# ------------------------------------------------------------------------------

logger.debug('Bye Bye!')
