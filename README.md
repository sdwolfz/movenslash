# Move'n Slash Game

## Client

Browser based client.

```sh
cd client
ruby -run -e httpd -- -b 0.0.0.0 -p 9090
```

## Proxy

TCP to UDP Proxy to connect from the browser.

```sh
cd proxy

bundle exec falcon serve -b http://localhost:4000 -n 1
```

## Server

UDP Server.

```sh
bundle exec ruby ./server/main.rb
```
