const ip   = '127.0.0.1'
const port = '4000'

const socket = new WebSocket(`ws://${ip}:${port}`)

// -----------------------------------------------------------------------------

let frequency = 200

let instance   = null
let nextAction = null

// -----------------------------------------------------------------------------

socket.addEventListener('open', (_event) => {
  const message = JSON.stringify({ action: 'state' })

  socket.send(message)
})

socket.addEventListener('message', (event) => {
  const reader = new FileReader()

  reader.onload = function (event) {
    instance = JSON.parse(event.target.result)
    console.log('Received message from server:', instance)

    draw(instance, canvas)
  }

  reader.readAsText(event.data)
})

socket.addEventListener('error', (event) => {
  console.error('WebSocket Error:', event)
})

socket.addEventListener('close', (event) => {
  if (event.wasClean) {
    console.log(`Closed cleanly, code=${event.code}, reason=${event.reason}`)
  } else {
    console.error('Connection died')
  }
})

// -----------------------------------------------------------------------------

const display = {
  game: document.getElementById('game'),
  canvas: null
}

display.game.innerHTML = ''

const canvas = document.createElement('canvas')
canvas.id = 'canvas';
canvas.width = 600;
canvas.height = 600;
display.canvas = canvas
display.game.appendChild(canvas)

// -----------------------------------------------------------------------------

function draw(instance, canvas) {
  const ctx = canvas.getContext('2d')

  clearCanvas(canvas, ctx)

  drawBackground(canvas, ctx)
  drawGrid(instance, canvas, ctx)

  drawCharacter(instance.player, 'green', canvas, ctx)

  instance.enemies.forEach((enemy) => {
    drawCharacter(enemy, 'red', canvas, ctx)
  })
}

function clearCanvas(canvas, ctx) {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function drawBackground(canvas, ctx) {
  ctx.fillStyle = 'gray'
  ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function drawGrid(instance, canvas, ctx) {
  const numRows = instance.map.x.max + 1
  const numCols = instance.map.y.max + 1

  const cellWidth = canvas.width / numCols
  const cellHeight = canvas.height / numRows

  ctx.beginPath()

  for (let index = 0; index <= numCols; index++) {
    const x = index * cellWidth

    ctx.moveTo(x, 0)
    ctx.lineTo(x, canvas.height)
  }

  for (let index = 0; index <= numRows; index++) {
    const y = index * cellHeight

    ctx.moveTo(0, y)
    ctx.lineTo(canvas.width, y)
  }

  ctx.strokeStyle = 'black'
  ctx.stroke()

  ctx.closePath()
}

function drawCharacter(character, color, canvas, ctx) {
  const numRows = instance.map.x.max + 1
  const numCols = instance.map.y.max + 1

  const cellWidth = canvas.width / numCols
  const cellHeight = canvas.height / numRows

  const centerX = (character.x + 0.5) * cellWidth
  const centerY = canvas.height - ((character.y + 0.5) * cellHeight)

  const arrowSize = cellWidth * 0.4

  ctx.beginPath()
  ctx.fillStyle = color

  // X is horizontal, from left to right
  // Y is vertical, from down to up

  switch (character.d) {
    case 'up':
      ctx.moveTo(centerX, centerY - arrowSize)
      ctx.lineTo(centerX - arrowSize, centerY + arrowSize)
      ctx.lineTo(centerX + arrowSize, centerY + arrowSize)
      break
    case 'down':
      ctx.moveTo(centerX, centerY + arrowSize)
      ctx.lineTo(centerX - arrowSize, centerY - arrowSize)
      ctx.lineTo(centerX + arrowSize, centerY - arrowSize)
      break
    case 'left':
      ctx.moveTo(centerX - arrowSize, centerY)
      ctx.lineTo(centerX + arrowSize, centerY - arrowSize)
      ctx.lineTo(centerX + arrowSize, centerY + arrowSize)
      break
    case 'right':
      ctx.moveTo(centerX + arrowSize, centerY)
      ctx.lineTo(centerX - arrowSize, centerY - arrowSize)
      ctx.lineTo(centerX - arrowSize, centerY + arrowSize)
      break
    default:
      break
  }

  ctx.fill()
  ctx.stroke()

  ctx.closePath()
}

// -----------------------------------------------------------------------------

const keys = [
  // Movement
  'h',
  'j',
  'k',
  'l',

  // Combat
  // ' '
]

const actions = {
  // Movement
  'h': 'left',
  'j': 'down',
  'k': 'up',
  'l': 'right',

  // Combat
  // ' ': 'attack'
}

document.addEventListener('keydown', (event) => {
  const key = event.key;

  if (keys.includes(key)) {
    nextAction = actions[key]
  }
})

// -----------------------------------------------------------------------------

console.log('Game START!')

setInterval(() => {
  let message = null

  if (nextAction) {
    message = JSON.stringify({ action: 'move', d: nextAction })
    nextAction = null
  } else {
    message = JSON.stringify({ action: 'state' })
  }

  socket.send(message)
  console.log('Sent: ', message)
}, frequency)
