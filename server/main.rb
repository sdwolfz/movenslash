#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'
require 'socket'
require 'timeout'
require 'logger'

# ------------------------------------------------------------------------------

logger = Logger.new($stdout)

# ------------------------------------------------------------------------------

ip   = '127.0.0.1'
port = 3000

socket = UDPSocket.new
socket.bind(ip, port)
logger.debug("Listening on #{ip}:#{port}")

# ------------------------------------------------------------------------------

require_relative './game_engine'
engine = GameEngine.new(logger: logger)

# ------------------------------------------------------------------------------

timeout = 200 / 1000.0

listeners = []
instruction = nil

loop do
  Timeout.timeout(timeout) do
    data, sender = socket.recvfrom(1024)
    listeners.push(sender) unless listeners.include?(sender)
    size = data.length

    logger.debug("Received #{size} bytes from #{sender[3]}:#{sender[1]}: #{data}")

    instruction = JSON.parse(data)
  end
rescue Timeout::Error
  engine.act(instruction) if instruction
  instruction = nil

  if listeners.any?
    response = JSON.dump(engine.state)
    logger.debug("Sending: #{response}")

    listeners.each do |listener|
      socket.send(response, 0, listener[3], listener[1])

      logger.debug("Sent state to #{listener[3]}:#{listener[1]}")
    end
    listeners = []
  else
    logger.debug('No listeners')
  end
rescue StandardError => e
  logger.debug("Error: #{e.message}")
end

# ------------------------------------------------------------------------------

socket.close
logger.debug('Bye Bye!')
