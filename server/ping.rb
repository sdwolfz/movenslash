#!/usr/bin/env ruby
# frozen_string_literal: true

require 'socket'

socket_ip   = '127.0.0.1'
socket_port = 3000

socket = UDPSocket.new
socket.connect(socket_ip, socket_port)

socket.send('ping', 0)

response, = socket.recvfrom(1024)

if response == 'pong'
  puts 'Server is responsive.'
else
  puts 'No response from the server.'
end

socket.close
