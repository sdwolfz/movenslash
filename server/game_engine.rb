# frozen_string_literal: true

class GameEngine
  # X is horizontal, from left to right
  # Y is vertical, from down to up

  INITIAL_STATE = {
    map: {
      x: {
        min: 0,
        max: 20
      },
      y: {
        min: 0,
        max: 20
      }
    },
    player: {
      x: 10,
      y: 10,
      d: 'down',
      hp: 1,
      id: 'player'
    },
    enemies: [
      {
        x: 0,
        y: 0,
        d: 'up',
        hp: 1,
        id: 'enemy 1'
      },
      {
        x: 20,
        y: 0,
        d: 'left',
        hp: 1,
        id: 'enemy 2'
      },
      {
        x: 0,
        y: 20,
        d: 'right',
        hp: 1,
        id: 'enemy 3'
      },
      {
        x: 20,
        y: 20,
        d: 'down',
        hp: 1,
        id: 'enemy 4'
      }
    ]
  }

  def initialize(logger:)
    @logger   = logger
    @instance = deep_clone(INITIAL_STATE)
  end

  def state
    deep_clone(@instance)
  end

  def act(instruction)
    case instruction['action']
    when 'move'
      direction = instruction['d']

      character = @instance[:player]
      possible  = validate_movement(character, direction)
      return unless possible

      move(character, direction)
    else
      false
    end
  end

  private

  def deep_clone(obj)
    Marshal.load(Marshal.dump(obj))
  end

  def validate_movement(character, direction)
    map = @instance[:map]

    case direction
    when 'left'
      character[:x] > map[:x][:min]
    when 'down'
      character[:y] > map[:y][:min]
    when 'up'
      character[:y] < map[:y][:max]
    when 'right'
      character[:x] < map[:x][:max]
    else
      false
    end
  end

  def move(character, action)
    case action
    when 'left'
      character[:x] -= 1
      character[:d] = 'left'
      @logger.debug("#{character[:id]}: Moved left x: #{character[:x]} y: #{character[:y]}")
    when 'down'
      character[:y] -= 1
      character[:d] = 'down'
      @logger.debug("#{character[:id]}: Moved down x: #{character[:x]} y: #{character[:y]}")
    when 'up'
      character[:y] += 1
      character[:d] = 'up'
      @logger.debug("#{character[:id]}: Moved up x: #{character[:x]} y: #{character[:y]}")
    when 'right'
      character[:x] += 1
      character[:d] = 'right'
      @logger.debug("#{character[:id]}: Moved right x: #{character[:x]} y: #{character[:y]}")
    else
      @logger.debug("#{character[:id]}: Invalid movement!")
    end
  end
end
